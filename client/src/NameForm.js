import React from 'react';
import axios from 'axios'

class NameForm extends React.Component {
    state = {companyName: ''};

   submitData = async () => {
        if (this.validLicense && this.financial.preTaxIncome) {
            this.submitContactData();
            this.submitFinancialData();
        }
    };

   loadData = async (license) => {
       if(license) {
           this.findPreviousContact(license);
           this.findPreviousFinancial(license)
       }
   }

    validLicense = () => {
        return !!(this.contact.license && this.contact.license.length > 7);
    };

    submitContactData = async () => {
        if (this.validLicense) {
            const self = this;
            axios.post('http://localhost:8080/contact/add', this.contact)
                .then((response) => {
                    self.contact.id = response.data;
                    self.disableLicense = true;
                })
                .catch(function (error) {
                    console.log(error);
                });

        }
    };

    submitFinancialData = async () => {
        if (this.financial.preTaxIncome && this.validLicense ) {
            const self = this;
            axios.post(`http://localhost:8080/finance/save?preTaxIncome=${self.financial.preTaxIncome}&license=${self.contact.license}`)
                .then((response) => {
                    self.contact.id = response.data;
                    self.disableLicense = true;
                    this.disableLicense = true;
                })
                .catch(function (error) {
                    console.log(error);
                });

        }
    };

    calculatepreTaxIncome = async () => {
            const self = this;
            axios.get(`http://localhost:8080/finance/calculate?preTaxIncome=${self.financial.preTaxIncome}`)
                .then((response) => {
                    self.setState({financial: response.data})
                })
                .catch(function (error) {
                    console.log(error);
                });
    };

    findPreviousContact = async (license) => {
        const self = this;
       axios.get(`http://localhost:8080/contact/find/${license}`)
            .then((response) => {
                if (response.data.id) {
                    self.setState({contact: response.data});
                    self.contact = response.data
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    };

    findPreviousFinancial = async (license) => {
        const self = this;
        axios.get(`http://localhost:8080/finance/${license}`)
            .then((response) => {
                if (response.data.preTaxIncome) {
                    console.log(response.data)
                    self.setState({financial: response.data});
                    self.financial = response.data
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    };

    constructor(props) {
        super(props);

        this.state = {
            contact: {},
            financial: {}
        }

        this.disableLicense = false;

        this.contact = {};
        this.financial = {};
    }

    render() {
        const isLicenseDisabled = () => {
            return this.state.contact.id
        };
        return (
            <div className="container mt-5">
                <h1 className="mb-5 mt-5">Onboarding Form</h1>
                <h2>Contact Details </h2>
                <form>
                    <form>
                        <div className="form-row">
                            <div className="form-group col-md-12">
                                <div className="form-group col-md-4">
                                    <label htmlFor="inputEmail4">License Number</label>
                                    <input
                                        className="form-control"
                                        type="text"
                                        defaultValue={this.state.contact.license}
                                        onChange={event => {
                                            this.contact.license = event.target.value
                                            this.loadData(event.target.value)
                                        }
                                        }
                                        disabled={this.state.contact.id || this.contact.id || isLicenseDisabled}
                                        placeholder="Enter License Number"
                                        required={true}
                                        minLength="8"
                                    />
                                </div>
                            </div>
                            <div className="form-group col-md-4">
                                <label htmlFor="inputEmail4">First Name</label>
                                <input type="text"
                                       required={true}
                                       className="form-control"
                                       id="firstName"
                                       onChange={event => {
                                           this.contact.firstName = event.target.value
                                       }
                                       }
                                       defaultValue={this.state.contact.firstName}
                                       placeholder="First Name"
                                />
                            </div>
                            <div className="form-group col-md-4">
                                <label htmlFor="inputEmail4">Middle Name</label>
                                <input type="text"
                                       className="form-control"
                                       id="middleName"
                                       onChange={event => {
                                           this.contact.middleName = event.target.value
                                         }
                                       }
                                       defaultValue={this.state.contact.middleName}
                                       placeholder="Middle Name"
                                />
                            </div>
                            <div className="form-group col-md-4">
                                <label htmlFor="inputEmail4">Last Name</label>
                                <input type="text"
                                       className="form-control"
                                       required={true}
                                       id="lastName"
                                       onChange={event => {
                                           this.contact.lastName = event.target.value
                                       }
                                       }
                                       defaultValue={this.state.contact.lastName}
                                       placeholder="Last Name"
                                />
                            </div>
                        </div>
                        <div className="form-group col-sm-3 align-items-sm-center">
                            <label htmlFor="inputAddress">Date Of Birth</label>
                            <input type="text"
                                   required={true}
                                   defaultValue={this.state.contact.dob}
                                   onChange={event => {
                                       this.contact.dob = event.target.value
                                   }
                                   }
                                   className="form-control"
                                   id="dateOfBirth"
                                   placeholder="DD/MM/YYYY"/>
                        </div>
                        <button type="button" onClick={this.submitContactData} className="btn btn-primary">Save
                            Contact Information
                        </button>
                    </form>
                    <h2 className="mb-2 mt-2">Financial Details </h2>
                        <div className="form-group col-md-4">
                            <label htmlFor="inputEmail4">Annual Taxable Income</label>
                            <input type="text"
                                   required={true}
                                   className="form-control"
                                   id="preTaxIncome"
                                   onChange={event => {
                                       this.financial.preTaxIncome = event.target.value;
                                       this.calculatepreTaxIncome();
                                   }
                                   }
                                   defaultValue={this.state.financial.preTaxIncome}
                                   placeholder="$AUD"
                            />
                        </div>
                        <div className="form-group col-md-4">
                            <label htmlFor="inputEmail4">Tax Paid</label>
                            <input type="text"
                                   className="form-control"
                                   id="taxPaid"
                                    disabled={true}
                                   defaultValue={this.state.financial.annualTax}
                                   placeholder="Tax Paid Annually"
                            />
                        </div>
                        <div className="form-group col-md-4">
                            <label htmlFor="inputEmail4">Net Income</label>
                            <input type="text"
                                   className="form-control"
                                   id="superannuation"
                                   disabled={true}
                                   defaultValue={this.state.financial.netIncome}
                                   placeholder="Net Income"
                            />
                        </div>
                        <div className="form-group col-md-4">
                            <label htmlFor="inputEmail4">Supperannuation Contribution</label>
                            <input type="text"
                                   className="form-control"
                                   id="superannuation"
                                   disabled={true}
                                   defaultValue={this.state.financial.superannuation}
                                   placeholder="Superannuation"
                            />
                        </div>
                    <button type="button" onClick={this.submitData} className="btn btn-primary">Save Onboarding Form</button>
                </form>
            </div>
        );
    }
}

export default NameForm