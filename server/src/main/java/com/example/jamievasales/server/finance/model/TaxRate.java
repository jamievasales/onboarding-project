package com.example.jamievasales.server.finance.model;

public class TaxRate {
    int baseTax;
    int lowerTaxLimit;
    double taxRatePerDollar;

    public TaxRate(int baseTax, int lowerTaxLimit, double taxRatePerDollar) {
        this.baseTax = baseTax;
        this.lowerTaxLimit = lowerTaxLimit;
        this.taxRatePerDollar = taxRatePerDollar;
    }

    public double getBaseTax() {
        return baseTax;
    }

    public int getLowerTaxLimit() {
        return lowerTaxLimit;
    }

    public double getTaxRatePerDollar() {
        return taxRatePerDollar;
    }
}
