package com.example.jamievasales.server.finance.model;

import io.swagger.annotations.ApiModelProperty;

public class FinancialDetail {
    @ApiModelProperty(notes = "Income pre tax", example = "25000", required = true, position = 1)
    private double preTaxIncome;
    @ApiModelProperty(notes = "Superannuation", example = "2450", hidden = false, position = 2)
    private double superannuation;
    @ApiModelProperty(notes = "Annual Tax", example = "1292", hidden = false, position = 3)
    private double annualTax;
    @ApiModelProperty(notes = "Net Income", example = "23708", hidden = false, position = 3)
    private double netIncome;

    public double getPreTaxIncome() {
        return preTaxIncome;
    }

    public void setPreTaxIncome(double preTaxIncome) {
        this.preTaxIncome = preTaxIncome;
    }

    public double getSuperannuation() {
        return superannuation;
    }

    public void setSuperannuation(double superannuation) {
        this.superannuation = superannuation;
    }

    public double getAnnualTax() {
        return annualTax;
    }

    public void setAnnualTax(double annualTax) {
        this.annualTax = annualTax;
    }

    public double getNetIncome() {
        return netIncome;
    }

    public void setNetIncome(double netIncome) {
        this.netIncome = netIncome;
    }
}
