package com.example.jamievasales.server.contact.controller;

import com.example.jamievasales.server.contact.manager.ContactManager;
import com.example.jamievasales.server.contact.model.Contact;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/contact")
@Api(value = "contact")
public class ContactController {

    @Autowired
    private ContactManager contactManager;

    @RequestMapping(
            value = {"/add"},
            method = RequestMethod.POST,
            produces = "application/json")
    @ResponseBody
    public ResponseEntity<String> addContact(@RequestBody Contact contact) {
        contactManager.saveContact(contact);
        return new ResponseEntity<>(contact.getId(), HttpStatus.OK);
    }

    @RequestMapping(
            value = {"/find/{license}"},
            method = RequestMethod.GET,
            produces = "application/json")
    @ResponseBody
    public ResponseEntity<Contact> getContact(@PathVariable("license") String license) {
        Contact contact = contactManager.findContactByLicense(license);
        return new ResponseEntity<>(contact, HttpStatus.OK);
    }

    @RequestMapping(
            value = {"/findall"},
            method = RequestMethod.GET,
            produces = "application/json")
    @ResponseBody
    public ResponseEntity<List<Contact>> getAllContacts() {
        List<Contact> contact = contactManager.findAll();
        return new ResponseEntity<>(contact, HttpStatus.OK);
    }


}
