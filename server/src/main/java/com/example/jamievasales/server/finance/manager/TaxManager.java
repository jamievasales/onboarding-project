package com.example.jamievasales.server.finance.manager;

import com.example.jamievasales.server.finance.model.TaxRate;
import com.example.jamievasales.server.finance.repository.TaxRepository;
import org.springframework.stereotype.Component;

@Component
public class TaxManager {
    private static final double TAX_FREE_TRESHOLD = 18200.00;

    public static TaxRate getTaxRate(double preTaxableIncome) {
        int taxBracket = 0;
        if (preTaxableIncome >= 18201 && preTaxableIncome <= 37000) {
            taxBracket = 1;
        } else if (preTaxableIncome >= 37001 && preTaxableIncome <= 90000) {
            taxBracket = 2;
        } else if (preTaxableIncome >= 90001 && preTaxableIncome <= 180000) {
            taxBracket = 3;
        } else {
            taxBracket = 4;
        }

        return TaxRepository.getTaxRate(taxBracket);
    }

    public static double calculateAnnualTax(double preTaxIncome) {
        double taxPaid;
        if (preTaxIncome <= TAX_FREE_TRESHOLD) {
            taxPaid = 0.0;
        } else {
            TaxRate taxRate = getTaxRate(preTaxIncome);
            taxPaid = taxRate.getBaseTax();
            taxPaid = taxPaid + ((preTaxIncome - taxRate.getLowerTaxLimit()) * (taxRate.getTaxRatePerDollar() / 100));
        }
        return Math.floor(taxPaid);

    }
}
