package com.example.jamievasales.server.finance.manager;

import com.example.jamievasales.server.finance.model.FinancialDetail;
import com.example.jamievasales.server.finance.repository.FinancalDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static org.apache.logging.log4j.util.Strings.isBlank;

@Component
public class FinancialDetailManager {
    @Autowired
    private FinancalDetailsRepository financalDetailsRepository;
    private static final double SUPERANNUATION_RATE = 0.098;

    public FinancialDetail saveFinancialDetail(double preTaxIncome, String license) {
        validate(preTaxIncome, license);
        FinancialDetail financialDetail = calaculateFinancialDetail(preTaxIncome);

        financalDetailsRepository.addOrUpdateContact(financialDetail, license);

        return financialDetail;
    }

    public FinancialDetail calaculateFinancialDetail(double preTaxIncome) {
        FinancialDetail financialDetail = new FinancialDetail();
        double annualTax = TaxManager.calculateAnnualTax(preTaxIncome);

        financialDetail.setPreTaxIncome(preTaxIncome);
        financialDetail.setAnnualTax(annualTax);
        financialDetail.setNetIncome(preTaxIncome - annualTax);
        financialDetail.setSuperannuation(Math.floor(preTaxIncome * SUPERANNUATION_RATE));

        return financialDetail;
    }

    public FinancialDetail getFinancialDetailByLicense(String license) {
        return financalDetailsRepository.findFinancialDetailByLicense(license);
    }

    private void validate(double preTaxIncome, String license) {
        if (isBlank(license) || preTaxIncome == 0) {
            throw new IllegalStateException("Mandatory fields are absent");
        }
    }
}
