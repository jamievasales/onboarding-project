package com.example.jamievasales.server.contact.repository;

import com.example.jamievasales.server.contact.model.Contact;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ContactRepository {
    private Map<String, Contact> contacts = new HashMap<>();

    public void addOrUpdateContact(Contact contact) {
        contacts.put(contact.getLicense(), contact);
    }

    public List<Contact> getAllContacts() {
        return new ArrayList<>(contacts.values());
    }

    public Contact findContactByLicense(String license) {
        return contacts.getOrDefault(license, null);
    }

    public void loadStubs() throws IOException {
        contacts = new ObjectMapper().readValue(new File("customers-stub.json"), contacts.getClass());
    }
}
