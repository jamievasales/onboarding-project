package com.example.jamievasales.server.contact.manager;

import com.example.jamievasales.server.contact.model.Contact;
import com.example.jamievasales.server.contact.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

import static org.apache.logging.log4j.util.Strings.isBlank;

@Component
public class ContactManager {

    @Autowired
    private ContactRepository contactRepository;

    public String saveContact(Contact contact) {
        validate(contact);
        contactRepository.addOrUpdateContact(contact);

        return contact.getLicense();
    }

    public Contact findContactByLicense(String license) {
        return contactRepository.findContactByLicense(license);
    }

    private void validate(Contact contact) {
        if (isBlank(contact.getFirstName()) || isBlank(contact.getLastName()) || isBlank(contact.getLicense())) {
            throw new IllegalStateException("Mandatory fields are absent");
        }
    }

    public List<Contact> findAll() {
        return contactRepository.getAllContacts();
    }
}
