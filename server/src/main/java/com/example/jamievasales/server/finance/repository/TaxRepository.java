package com.example.jamievasales.server.finance.repository;

import com.example.jamievasales.server.finance.model.TaxRate;

import java.util.HashMap;

public class TaxRepository {
    private static final HashMap<Integer, TaxRate> taxRate = new HashMap<>();

    static {
        taxRate.put(0, new TaxRate(0, 0, 0));
        taxRate.put(1, new TaxRate(0, 18200, 19));
        taxRate.put(2, new TaxRate(3572, 37000, 32.5));
        taxRate.put(3, new TaxRate(20797, 90000, 37));
        taxRate.put(4, new TaxRate(54097, 180000, 45));
    }

    private TaxRepository() {
    }

    public static TaxRate getTaxRate(int bracket) {
        if (bracket < taxRate.size()) {
            return taxRate.get(bracket);
        }

        return taxRate.get(0);
    }
}
