package com.example.jamievasales.server.finance.repository;

import com.example.jamievasales.server.finance.model.FinancialDetail;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Component
public class FinancalDetailsRepository {
    private final Map<String, FinancialDetail> financialDetails = new HashMap<>();

    public void addOrUpdateContact(FinancialDetail financialDetail, String license) {
        financialDetails.put(license, financialDetail);
    }

    public ArrayList<FinancialDetail> getAllFinancialDetails() {
        return new ArrayList<>(financialDetails.values());
    }

    public FinancialDetail findFinancialDetailByLicense(String license) {
        return financialDetails.getOrDefault(license, null);
    }
}
