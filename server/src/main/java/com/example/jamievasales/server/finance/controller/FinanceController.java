package com.example.jamievasales.server.finance.controller;

import com.example.jamievasales.server.finance.manager.FinancialDetailManager;
import com.example.jamievasales.server.finance.model.FinancialDetail;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/finance")
@Api(value = "finance")
public class FinanceController {

    @Autowired
    private FinancialDetailManager financialDetailManager;

    @RequestMapping(
            value = {"/save"},
            method = RequestMethod.POST,
            produces = "application/json")
    @ResponseBody
    public ResponseEntity<FinancialDetail> saveFinancialDetails(@RequestParam("preTaxIncome") double preTaxIncome, @RequestParam("license") String license) {
        FinancialDetail financialDetail = financialDetailManager.saveFinancialDetail(preTaxIncome, license);
        return new ResponseEntity<>(financialDetail, HttpStatus.OK);
    }

    @RequestMapping(
            value = {"/calculate"},
            method = RequestMethod.GET,
            produces = "application/json")
    @ResponseBody
    public ResponseEntity<FinancialDetail> calculateFinancialDetail(@RequestParam("preTaxIncome") double preTaxIncome) {
        FinancialDetail financialDetail = financialDetailManager.calaculateFinancialDetail(preTaxIncome);
        return new ResponseEntity<>(financialDetail, HttpStatus.OK);
    }

    @RequestMapping(
            value = {"/{license}"},
            method = RequestMethod.GET,
            produces = "application/json")
    @ResponseBody
    public ResponseEntity<FinancialDetail> getFinancialDetail(@PathVariable("license") String license) {
        FinancialDetail financialDetail = financialDetailManager.getFinancialDetailByLicense(license);
        return new ResponseEntity<>(financialDetail, HttpStatus.OK);
    }
}
