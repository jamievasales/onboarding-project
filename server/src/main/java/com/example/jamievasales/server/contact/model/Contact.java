package com.example.jamievasales.server.contact.model;

import io.swagger.annotations.ApiModelProperty;

import java.util.UUID;

import static org.apache.logging.log4j.util.Strings.EMPTY;
import static org.apache.logging.log4j.util.Strings.isNotBlank;

public class Contact {
    @ApiModelProperty(notes = "Unique identifier of the person. No two persons can have the same id.", example = "1", hidden = true)
    private final String id;
    @ApiModelProperty(notes = "First name of the contact.", example = "John", required = true, position = 1)
    private String firstName;
    @ApiModelProperty(notes = "Last name of the contact.", example = "Doe", required = true, position = 2)
    private String lastName;
    @ApiModelProperty(notes = "Middle name of the person.", example = "Smith", position = 3)
    private String middleName;
    @ApiModelProperty(notes = "License as a primary form of ID", example = "2009AB123", position = 4)
    private String license;
    @ApiModelProperty(notes = "Date of Birth", example = "21/01/1990", required = true, position = 5)
    private String dob;

    public Contact() {
        this.id = UUID.randomUUID().toString();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getDob() {
        return dob;
    }

    public String getId() {
        return id;
    }

    public String getLicense() {
        return license;
    }

    public static class PersonBuilder {
        private String firstName;
        private String lastName;
        private String middleName;
        private String license;
        private String dob;

        public PersonBuilder withContact(Contact contact) {
            this.license = contact.license;
            this.firstName = contact.firstName;
            this.middleName = contact.middleName;
            this.lastName = contact.lastName;
            this.dob = contact.dob;

            return this;
        }

        public PersonBuilder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public PersonBuilder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public PersonBuilder withMiddleName(String middleName) {
            this.middleName = middleName;
            return this;
        }

        public PersonBuilder withDob(String dob) {
            this.dob = dob;
            return this;
        }

        public PersonBuilder withLicense(String license) {
            this.license = license;
            return this;
        }

        public Contact build() {
            Contact contact = new Contact();
            contact.firstName = firstName;
            contact.lastName = lastName;
            contact.middleName = isNotBlank(middleName) ? middleName : EMPTY;
            contact.license = license;
            contact.dob = dob;

            return contact;
        }
    }
}
