package com.example.jamievasales.server.finance.repository;

import com.example.jamievasales.server.finance.model.TaxRate;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class TaxRepositoryTest {

    @Test
    void valid_tax_rate_returns_tax_bracket() {
        TaxRate taxRate = TaxRepository.getTaxRate(3);
        assertEquals(taxRate.getBaseTax(), 20797);
    }

    @Test
    void out_of_bounds_tax_bracket_returns_0() {
        assertEquals(TaxRepository.getTaxRate(0),TaxRepository.getTaxRate(100));
    }
}