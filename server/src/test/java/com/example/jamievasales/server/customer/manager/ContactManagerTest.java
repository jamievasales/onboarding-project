package com.example.jamievasales.server.customer.manager;

import com.example.jamievasales.server.contact.model.Contact;
import com.example.jamievasales.server.contact.manager.ContactManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class ContactManagerTest {
    @Autowired
    ContactManager contactManager;

    Contact aSavedInvalidContact = new Contact.PersonBuilder()
            .withFirstName("Jamie")
            .withLastName("Vasales")
            .withDob(null)
            .build();

    @BeforeEach
    void setUp() {
        contactManager = new ContactManager();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    public void add_new_person_with_missing_parameters_throws_ISE() {
        assertThrows(IllegalStateException.class, () -> contactManager.saveContact(aSavedInvalidContact));
    }
}