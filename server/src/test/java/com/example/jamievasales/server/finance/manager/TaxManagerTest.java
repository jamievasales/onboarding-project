package com.example.jamievasales.server.finance.manager;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class TaxManagerTest {
    @Autowired
    private TaxManager taxManager;

    @Test
    void given_income_tax_free_threshold_return_annualTax_of_0() {
        double INCOME = 12000;
        assertEquals(0,taxManager.calculateAnnualTax(INCOME));
    }

    @Test
    void given_income_36001_50c_return_annualTax_of_3382() {
        double INCOME = 36001;
        assertEquals(3382.00,taxManager.calculateAnnualTax(INCOME));
    }

    @Test
    void given_income_90k_return_annualTax_of_25717() {
        double INCOME = 90000;
        assertEquals(20797.0,taxManager.calculateAnnualTax(INCOME));
    }

    @Test
    void given_income_100k_return_annualTax_of_24497() {
        double INCOME = 100000;
        assertEquals(24497.0,taxManager.calculateAnnualTax(INCOME));
    }

    @Test
    void given_income_250k_return_annualTax_of_24497() {
        double INCOME = 250005.65;
        assertEquals(85599.00,taxManager.calculateAnnualTax(INCOME));
    }


}