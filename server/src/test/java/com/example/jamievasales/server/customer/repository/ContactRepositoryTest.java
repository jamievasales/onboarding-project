package com.example.jamievasales.server.customer.repository;

import com.example.jamievasales.server.contact.model.Contact;
import com.example.jamievasales.server.contact.repository.ContactRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class ContactRepositoryTest {
    private ContactRepository contactRepository;
    private static final String LICENSE = "2009ABC123";
    private static final Contact A_SAVED_VALID_CONTACT = new Contact.PersonBuilder()
            .withFirstName("Jamie")
            .withLastName("Vasales")
            .withLicense(LICENSE)
            .withDob(null)
            .build();

    private static final Contact ANOTHER_SAVED_VALID_CONTACT = new Contact.PersonBuilder()
            .withFirstName("Customer")
            .withLastName("two")
            .withLicense(LICENSE)
            .withDob(null)
            .build();

    @Test
    public void when_add_customer_expect_size_1() {
        contactRepository = new ContactRepository();
        contactRepository.addOrUpdateContact(A_SAVED_VALID_CONTACT);
       assertEquals(contactRepository.getAllContacts().size(),1);
    }

    @Test
    public void when_add_multiple_customer_expect_size_2() {
        contactRepository = new ContactRepository();
        contactRepository.addOrUpdateContact(A_SAVED_VALID_CONTACT);
        contactRepository.addOrUpdateContact(ANOTHER_SAVED_VALID_CONTACT);
        assertEquals(contactRepository.getAllContacts().size(),1);
    }

    @Test
    public void when_update_customer_return_updated_customer() {
        contactRepository = new ContactRepository();
        contactRepository.addOrUpdateContact(A_SAVED_VALID_CONTACT);
        Contact updatedContact = new Contact.PersonBuilder().withContact(A_SAVED_VALID_CONTACT).withLastName("Test").build();
        contactRepository.addOrUpdateContact(updatedContact);
        assertEquals(contactRepository.getAllContacts().size(),1);
        assertEquals(contactRepository.getAllContacts().get(0).getLastName(),"Test");
    }

    @Test
    public void when_load_stubs_expect_3_results() throws IOException {
        contactRepository = new ContactRepository();
        contactRepository.loadStubs();
        assertEquals(contactRepository.getAllContacts().size(),3);
    }

    @Test
    public void when_finding_added_customer_by_license_return_customer() throws IOException {
        contactRepository = new ContactRepository();
        contactRepository.addOrUpdateContact(A_SAVED_VALID_CONTACT);
        assertNotNull(contactRepository.findContactByLicense(LICENSE));
    }

    @Test
    public void when_finding_missing_customer_by_license_return_null() throws IOException {
        contactRepository = new ContactRepository();
        assertNull(contactRepository.findContactByLicense(LICENSE));
    }

}