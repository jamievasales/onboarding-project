# OnboardingElectronicForm

A 'mock up' of mostly the backend of a simple onboarding form for a bank. ****Customer should be renamed to reflect the problem, A revision would refactor all references of customer to contact****

##Introduction:
At the time of writing this will be a backend focused onboarding system with a tax calculator. Due to time constraints and other priorities we will be using `HashMaps with a single key` as a makes shift database

Swagger will allow for the easy use of our api in light of not having the best front end

Whilst there are tests included in the project the controllers have no associated tests. This is a direct result of time and availability to fulfill this project in a limited amount of time

**Many form fields are missing to make the form brief and keep in line with other commitments**

##Approach taken: 
The project utilises the Spring Framework (Spring Boot) to allow for rapid development and deployment of our tech demo. Ideally a DB would have been included but for the purpose of a simple demo a HashMap will suffice

Junit 5 was used as the testing framework as a result of its ease of use and it being easily read. More testing would ideally be added

The front end is react based with a quick mock up being chosen over good coding practices.
 -- The expected functionality was that a user can save their details to later return to the form and complete is based on their license. Additonal security would obviously be used in a real scenario

 Mocks were intended to be used to add data quickly for testing purposes if changes are to be made. This was not fully realised so that other requirements can be met

##How to run/test

To run this project simply clone the repository, open the pom.xml as a project and run.

The front end can be run via `npm start` using node v12 (after `npm install` is run of course)

Swagger is available via http://localhost:8080/swagger-ui/



##Assumptions:
`1.` We do not care too much about the exact functionality of the front end. Whilst we can obtain previously saved details we would do it in a more elegant way
   
`2` We are going to assume that anything we do from the UI we can also do from the API. At the time of writing the UI is not complete so a sample form, swagger and imagination will be used.

`3` Super is calculated by a static value

`4` This is purely a tech demo. In the real world this would be drastically different. Choices made are a direct result of time and availability

`5` We are assuming our users are responsible and give valid values within a reasonable range for the demo. We WOULD NOT allow out of range values in the future. We would validate values and provide appropriate error messages